
## Guide on local development setup

### 1. install composer
```
docker run --rm -v $(pwd):/app composer install
```

### 2. Change ownership
```
sudo chown -R $USER:$USER ~/art-sites-demo
```

### 3. Copy local env
```
cp .env.example .env
```

### 4. Update local .env
```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=laraveluser
DB_PASSWORD=your_laravel_db_password

```

### 4. Artisan preparation

```
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan config:cache

```

## References

### build php 7.4:
```
http://dimonz.ru/post/create-php-7-4-docker-image

```


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
